mm = load('output.txt');
minutes=unique(mm(:,1))';

figure(1);
clf; hold on; grid on;
for (m = minutes(10:20))
    f=mm(:,1) == m;
    h=plot(mm(f, 2), mm(f, 3));
    % legend would make the graph slightly unreadable
    % legend(h, sprintf('%d', m));
end
