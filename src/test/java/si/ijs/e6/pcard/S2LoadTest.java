package si.ijs.e6.pcard;

import org.junit.Before;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.io.PrintWriter;

/**
 * This class contains the unit test for S2Load
 */
public class S2LoadTest {

    private void setupLogger() {
        // remove the default root logger
        LogManager.getLogManager().reset();
        // ###########################################################################################################
        // setup logging
        //System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
        System.setProperty("java.util.logging.SimpleFormatter.format", "%4$s %2$s %5$s%6$s%n"); //removed 'time'
        // redirect all the logs to console and set filter level
        final Logger logger = Logger.getLogger("");
        ConsoleHandler consoleHandler = new ConsoleHandler();
        //logger.addHandler(consoleHandler);
        //consoleHandler.setLevel(Level.INFO);
    }

    @Before
    public void setUp() throws Exception {
        setupLogger();
    }

    /**
     * This test is used to test limitation (start and end times) to s2 loading.
     */
    @Test
    public void testTimeLimits() {
        String loadFilenameDataStream = "./src/test/test_measurements/cenversionInput.s2";
        // this file's last samples are at 11081.67s

        loadFile(loadFilenameDataStream, 0, 0);
        loadFile(loadFilenameDataStream, 0, 1);
        loadFile(loadFilenameDataStream, 1, 2);
        loadFile(loadFilenameDataStream, 2, 3);
        loadFile(loadFilenameDataStream, 20, 30);
        loadFile(loadFilenameDataStream, 11081, 11082);
        loadFile(loadFilenameDataStream, 11082, 11083);
        loadFile(loadFilenameDataStream, 500, 5000);

        loadFile("/home/matjaz/Android/projects/MobECG/last.s2", 0, 5000);
    }


    /**
     * This test will load several intervals (partly overlapping) and will export them to a txt file.
     * Resulting txt fiel can be opened in octave and plotted, to see how well do the intervals
     * really overlap and thus how sensitive the timeAlignment is to interval selection
     *
     * Warning: this will create a long file, expect over 10MB for an hour of measurement!
     */
    @Test
    public void loadAndOutputOverlappingIntervals() {
        String loadFilenameDataStream = "./src/test/test_measurements/cenversionInput.s2";
        String outputFname = "output.txt";
        System.out.printf("This function will store its results to %s", outputFname);
        try {
            PrintWriter print = new PrintWriter(new FileWriter(outputFname));
            for (int minutes = 1; minutes < 32; ++minutes) {
                IterativeBlockLoad ret = loadFileOverlap(loadFilenameDataStream, (minutes - 1) * 60, minutes * 60);
                float[] samples = ret.getSamples();
                long[] nanos = ret.getTimestamps();
                for (int i = 0; i < samples.length; ++i) {
                    print.printf("%d %.6f %.3f %n", minutes, nanos[i]*1e-9, samples[i]);
                }
                System.out.printf("%d ", samples.length);
            }
            print.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected IterativeBlockLoad loadFileOverlap(String fname, float from, float to) {
        return loadFile(fname, Math.max(0, from-10), to+10, true);
    }

    protected IterativeBlockLoad loadFile(String fname, float from, float to) {
        return loadFile(fname, from, to, false);
    }

    protected IterativeBlockLoad loadFile(String fname, float from, float to, boolean returnSamples) {
        S2Load loader = new S2Load(fname);
        loader.limitInputTimeInterval(from, to);
        // make the chunk size smaller tnan the interval (will assume at least 128 samples per second)
        loader.setTargetNumberOfSamplesPerIteration(128*20);

        // todo: check if file was found
        IterativeBlockLoad callback = new IterativeBlockLoad();

        if (loader.scan(callback) == null)
            return callback;

        if (returnSamples)
            callback.enableStorageOfSamples();

        loader.convertLoop(callback);
        float loadedLength = callback.countSamples[0]/callback.samplingFrequency [0];

        // test the length of loaded interval against the expected max interval
        if ((to - from) < loadedLength) {
            if (to - from <= 0) {
                System.out.printf("Requested load of entire file; \n" +
                                "   -> %.3f seconds were loaded\n" + "   -> from %.3f to %.3f\n",
                        from, to, loadedLength, callback.timestamps[0] * 1e-9, callback.timestamps[1] * 1e-9);
            } else {
                // loaded more than requested should be impossible, this is an error in implementation
                System.err.printf("Error in loading from %.3f to %.3f; -> %.3f seconds were loaded\n",
                        from, to, loadedLength);
            }
        } else {            // everything is ok
            System.out.printf("Requested load from %.3f to %.3f; \n" +
                    "   -> %.3f seconds were loaded\n" + "   -> from %.3f to %.3f\n",
                    from, to, loadedLength, callback.timestamps[0]*1e-9, callback.timestamps[1]*1e-9);
        }

        return callback;
    }

}