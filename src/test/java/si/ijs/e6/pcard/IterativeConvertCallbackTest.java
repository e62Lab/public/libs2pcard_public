package si.ijs.e6.pcard;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class IterativeConvertCallbackTest {

    private void setupLogger() {
        // remove the default root logger
        LogManager.getLogManager().reset();
        // ###########################################################################################################
        // setup logging
        //System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
        System.setProperty("java.util.logging.SimpleFormatter.format", "%4$s %2$s %5$s%6$s%n"); //removed 'time'
        // redirect all the logs to console and set filter level
        final Logger logger = Logger.getLogger("");
        Handler consoleHandler = new ConsoleHandler();
        logger.addHandler(consoleHandler);
        consoleHandler.setLevel(Level.WARNING);
    }


    /**
     * Callback for implementing conversion for use in S2Load
     * Output of converted s2 will be printed to console
     *
     */
    class ConsoleConvertCallback implements S2Load.IterativeConvertCallback {
        /**
         * this variable holds the record (measurement) start date and time
         */
        private Calendar recordStart;
        /**
         * Conversion multiplier and offset from input samples to destination values
         */
        private LinearTransformationFunction ltf;
        /**
         * User-defined constant - how many samples to output to console
         */
        private final int maxOutputSamples = 100;
        /**
         * Variable that keeps track of already output samples
         */
        private int countSamples = 0;
        /**
         * Use this function to setup start date and time of the measurement.
         * @param startDateTime date and time (including milliseconds)
         */
        public void setRecordStartDateTIme(GregorianCalendar startDateTime) {
            recordStart = startDateTime;
        }

        @Override
        public float selectFrequency(float inputFrequency) {
            /*
                This function is called by S2Load, once the frequency of s2 input is known.
                To leave the frequency unmodified, just return the same value
             */
            return inputFrequency;
        }

        /**
         * Debugging tool
         * @param date date to print
         */
        void printCalendar(Calendar date) {
            System.out.print(date.get(Calendar.YEAR)+"-"+(date.get(Calendar.MONTH)+1)+"-"+date.get(Calendar.DAY_OF_MONTH));
            System.out.print(" "+date.get(Calendar.HOUR)+":"+date.get(Calendar.MINUTE)+":"+date.get(Calendar.SECOND));
            System.out.println(" +"+date.get(Calendar.MILLISECOND));
        }

        @Override
        public void onInitialization(S2Load.FileScanResults scanResult) {
            /*
                This function is called by S2Load after the input file has been scanned
             */

            // prepare transformation function for measurement amplitude
            ltf = scanResult.linearTransformationFunction;

            // parse metadata of s2 and form metadata for output
            String gadgetId = scanResult.metadata.get(S2PcardFile.Metadata.Mac_address.getString());
            String gadgetHardwareString = scanResult.gadgetFirmwareAndHardware.toString();
            String typeOfRecorderArr[] = scanResult.getShortHardwareDescriptionString().split("\n");
            String typeOfRecorder = "";
            for (String field : typeOfRecorderArr)
                typeOfRecorder = typeOfRecorder + (typeOfRecorder.isEmpty() ? "[ " : "; ")+field;
            typeOfRecorder = typeOfRecorder + " ]";

            // setup record start time from the time obtained from metadata and offset into the measurement
            recordStart = scanResult.recordDateTime;

            // TODO deal better with the possibility of invalid date in s2
            if (recordStart == null)
                recordStart = new GregorianCalendar(2700, Calendar.JANUARY, 31, 12, 59, 59);

            long millisecondsOffset = (scanResult.startTimeNanos / 1_000_000);
            final int millis_in_a_day = (1000*3600*24);
            long daysOffset = millisecondsOffset / millis_in_a_day;
            millisecondsOffset -= daysOffset * millis_in_a_day;
            recordStart.add(Calendar.MILLISECOND, (int) millisecondsOffset);
            recordStart.add(Calendar.DATE, (int) daysOffset);

            // start writing to output, measurement metadata first
            System.out.println("Measurement info:");
            System.out.println("   gadget = "+gadgetId);
            System.out.println("   gadget hardware (as written in s2) = "+gadgetHardwareString);
            System.out.println("   type of recorder = "+typeOfRecorder);
            System.out.println("   record date = "+recordStart.get(Calendar.YEAR)+"-"+(recordStart.get(Calendar.MONTH)+1)+"-"+recordStart.get(Calendar.DAY_OF_MONTH));
            System.out.println("   record start time = "+recordStart.get(Calendar.HOUR)+":"+recordStart.get(Calendar.MINUTE)+":"+recordStart.get(Calendar.SECOND)+" +"+recordStart.get(Calendar.MILLISECOND)+" ms");
            System.out.println("   record sampling frequency = "+scanResult.inputFrequency+" Hz");
            System.out.println("   transformation function: y="+ltf.getK()+" x "+(ltf.getN() >= 0 ? "+ " : "- ")+Math.abs(ltf.getN()));
            // annotations are also loaded during the scan and are already available
            if (scanResult.annotations.isEmpty()) {
                System.out.println("Annotations: none");
            } else {
                System.out.println("Annotations:");
                for (S2Load.AnnotationData ann : scanResult.annotations) {
                    // timestamp is in nanoseconds relative to the measurement start, message is plain text
                    System.out.println("   @ "+ann.timestamp*1e-9+" s: "+ann.message);
                }
            }
            // data will be loaded later (can be processed in onIteration callback), prepare console output for them
            System.out.println("Measurement data [mV]:");
            System.out.print("   ");
        }

        @Override
        public boolean onIteration(float[] samples) {
            /*
                This function is called on each iteration of S2Load, that is, after a sufficiently large block of data is read
             */
            for (float sample : samples) {
                // output the sample to console
                System.out.print(ltf.transformFloat(sample)+" ");
                // Count number of processed samples and stop conversion if enough has been processed
                countSamples++;
                if (countSamples > maxOutputSamples)
                    return false; // stop conversion
            }
            return true; // request the conversion process to continue
        }

        @Override
        public void complete() {
            /*
                This function is called once the loading has finished
             */
            System.out.println();
            System.out.println("End");
        }
    }

    @Before
    public void before() {
        setupLogger();
    }

    /**
     * An example of use of custom @{link S2Load.IterativeCallback} function
     */
    @Test
    public void exampleOfUse() {
        // set a path to an existing file here:
        String loadFilenameDataStream = "./src/test/test_measurements/cenversionInput.s2";

        // prepare S2Load instance
        S2Load instance = new S2Load(loadFilenameDataStream);
        // [optional] limit the time interval for loading (seconds)
        instance.limitInputTimeInterval(0, 1300);
        // todo: check if file was found

        // prepare callback that will printout the loaded data
        ConsoleConvertCallback callback = new ConsoleConvertCallback();
        // process the file (scan & load, calling the appropriate callback when needed)
        if (instance.scan(callback) == null) {
            System.out.println("Scan of file "+loadFilenameDataStream+" returned null, cannot proceed,");
            return;
        }
        instance.convertLoop(callback);
    }

}
