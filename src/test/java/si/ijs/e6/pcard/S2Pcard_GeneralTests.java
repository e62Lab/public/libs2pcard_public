package si.ijs.e6.pcard;

import org.junit.Test;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import si.ijs.e6.MultiBitBuffer;
import si.ijs.e6.S2;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static si.ijs.e6.pcard.S2PcardFile.*;

/**
 * S2 PCARD file storage tests
 */
public class S2Pcard_GeneralTests {
    private S2 s2LoadFile;
    private S2.StoreStatus s2storeStatus;
    private S2StreamType s2StreamType;
    private LinearTransformationFunction ltf;
    S2.TimestampDefinition.TimestampFormatter tsf;

    /**
     * Test of basic measurement storage to file
     *
     * Tests
     *  - Usage of constants from pcard.S2PcardFile.* and S2PcardFile.MetadataStrings.*
     *  - Usage of S2StoreStatus
     *  - Usage of S2.TimestampDefinition.TimestampFormatter (feed it with short and long pauses)
     */
    @Test
    public void testStoringToPcardS2_NormalStream() throws Exception {
        s2StreamType = S2StreamType.NormalStream;
        createAndInitS2File("/tmp/normalStream.s2");

        fillWithFakeData(false);
        s2storeStatus.done();
        // external look into the file is required
    }

    /**
     * Test basic storing a measurement to file using extended stream
     */
    @Test
    public void testStoringToPcardS2_ExtStream() throws Exception {
        s2StreamType = S2StreamType.ExtendedStream_0;
        createAndInitS2File("/tmp/extStream.s2");

        fillWithFakeData(false);
        s2storeStatus.done();
        // external look into the file is required
    }

    private void fillWithFakeData(boolean randomizeTimes) {
        // new buffer to store timestamp and samples (rawdata)
        byte buffer[] = new byte[255];
        int dataLength = 19;
        long fakeTimestamp = 0;
        long counter = 0;
        for (int i = 0; i < 100; ++i) {
            // copy rawdata to buffer
            MultiBitBuffer mbb = new MultiBitBuffer(buffer);
            int bitIndex = 8 * 3;
            for (int si = 0; si< s2StreamType.getNumSamplesPerPacket(); ++si) {
                mbb.setInts((int)(200+200*Math.sin((i * s2StreamType.getNumSamplesPerPacket() + si) * 0.02)),
                        bitIndex, s2StreamType.getSampleBitWidth(), 1);
                bitIndex += s2StreamType.getSampleBitWidth();
            }
            mbb.setInts((int)(counter%1024), bitIndex, s2StreamType.getCounterBitWidth(), 1);
            counter += s2StreamType.getCounterIncrement();

            // format timestamp and copy it to the buffer
            if (!tsf.formatForStorage(fakeTimestamp, buffer, false)) {
                // write the timestamp line before the data stream line
                s2storeStatus.addTimestamp(new S2.Nanoseconds(fakeTimestamp));
            }
            s2storeStatus.addSensorPacket((byte) 0, buffer, tsf.getTimestampLength() + dataLength);

            if (randomizeTimes)
                fakeTimestamp += 1000L*256*256*256*Math.random();
            else
                fakeTimestamp += s2StreamType.getNumSamplesPerPacket()*0.005e9;
        }

        assertThat(s2LoadFile.getNumErrors(), is(0));
    }

    private void createAndInitS2File(String outputFilename) {
        System.out.println("Creating test file "+outputFilename);
        s2LoadFile = new S2();
        s2storeStatus = s2LoadFile.store(new File(outputFilename));
        s2storeStatus.setVersion(Version.targetBaseVersion, Version.targetExtendedVersion).addMetadata(Metadata.Recording_device_software.toString(), "Unit test");

        // firstReceived time is the start of the measurement - this way the first arrived data will
        // have offset from the measurement start as its timestamp, as it should
        long firstReceivedTime = System.nanoTime();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

        s2storeStatus
                .addMetadata(Metadata.Sensing_gadget_hardware.toString(), "Fake sensing gadget 0.0.1")
                .addMetadata(Metadata.Mac_address.toString(), "FA:KE:AD:DR:ES:S0")
                .addMetadata(Metadata.Recording_date.toString(), dateFormat.format(new Date()))
                .addMetadata(Metadata.Recording_time.toString(), timeFormat.format(new Date()))
                .addMetadata(Metadata.Recording_timezone.toString(), "+0100");

        s2storeStatus.addMetadata(Metadata.Selected_adc_channel.toString(), "ECG_OUT");
        s2storeStatus.addMetadata(Metadata.Debug_selected_analog_ampl.toString(), "1");
        s2storeStatus.addMetadata(Metadata.Debug_selected_ecg_range.toString(), "6 mV");

        ltf = new LinearTransformationFunction(5.9e-3f, -3.04f);
        s2storeStatus.addMetadata(Metadata.Debug_calibration_coeffs.toString(),
               String.format(Locale.US, "%.2f, %.2f", -ltf.getOffset()/ltf.getAmplitude(),
               2/ltf.getAmplitude()));

        if (s2StreamType == S2StreamType.NormalStream)
            s2storeStatus.addMetadata(Metadata.Selected_transfer_protocol.toString(), "NORMAL_STREAMING_PROTOCOL");
        if (s2StreamType == S2StreamType.ExtendedStream_0)
            s2storeStatus.addMetadata(Metadata.Selected_transfer_protocol.toString(), "EXTENDED_STREAMING_PROTOCOL_No_0");
        s2storeStatus.addMetadata(Metadata.Sensing_gadget_location.toString(), "1: 0 /undefined)");
        s2storeStatus.addMetadata(Metadata.Think_ehr_id.toString(), "/");
        s2storeStatus.addMetadata(Metadata.Recording_device_model.toString(), "PC");
        s2storeStatus.addMetadata(Metadata.Recording_device_os.toString(), System.getProperty("os.name"));
        s2storeStatus.addMetadata(Metadata.Recording_device_serial_number.toString(), "0123456789");

        Map<Byte, S2.SensorDefinition> sds = s2StreamType.generateSensorDefinitions(
                ltf, 200);
        for (Map.Entry<Byte, S2.SensorDefinition> sd: sds.entrySet()) {
            s2storeStatus.addDefinition(sd.getKey(), sd.getValue());
            System.out.printf("entry %c: f="+sd.getValue().samplingFrequency+"\n", sd.getKey());
        }
        byte handle = s2StreamType.getStructDefinitionHandle();
        s2storeStatus.addDefinition(handle, s2StreamType.generateStructDefinition());
        S2.TimestampDefinition tsd = s2StreamType.generateTimestampDefinition();
        tsf = tsd.newTimestampFormatter();
        s2storeStatus.addDefinition(handle, tsd);

        assertThat(s2LoadFile.getNumErrors(), is(0));
    }
}
