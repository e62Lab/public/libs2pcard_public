package si.ijs.e6.pcard;

import org.junit.Test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import si.ijs.e6.S2;

import static org.junit.Assert.assertTrue;


/**
 * This class contains tests for writing PCARD s2 files
 * The output of this program can be used to basically test converters. Output contains sensible
 * data (ECG-like), regular frequency, and holes (missing data)
 *
 *
 * This class was written to mimic the way s2 files are created in MobECG
 */
public class S2PcardFileTest {
    S2 s2file;
    S2.StoreStatus s2storeStatus;
    S2.TimestampDefinition timestampDefinitionForDataStream;
    S2.TimestampDefinition.TimestampFormatter ecgTimestampFormatter;
    long firstReceivedTime = 0;
    // the boolean used to manually write timestamps when necessary
    boolean writeTimestampBeforeSamples = true;
    float samplingFrequency = 128;
    LinearTransformationFunction ltf = new LinearTransformationFunction(0.006f, -3.072f);
    // reusable variable to hold data (initialized in function init)
    UnpackedEcgData data = new UnpackedEcgData();
    // reusable rawData buffer
    byte[] rawData = new byte[19];
    // reusable multi bit buffer on top of rawData
    MultiBitBuffer mbb = new MultiBitBuffer(rawData);
    // delta time between packets
    long dt;
    // synthetic ECG
    double[] syntheticECG = {0.0000, -0.0000, -0.0000, 0.0007, 0.0011, 0.0005, -0.0005, -0.0011, 0.0351, 0.0966, 0.1484, 0.1885, 0.2140, 0.2283, 0.2291, 0.2115, 0.1782, 0.1308, 0.0691, 0.0045, 0.0003, 0.0004, 0.0012, 0.0011, -0.0005, -0.0017, -0.0001, 0.0693, 0.3006, 0.5277, 0.7545, 0.9832, 1.2121, 1.4370, 1.3085, 1.0797, 0.8544, 0.6285, 0.4003, 0.1725, -0.0071, -0.0576, -0.1143, -0.1748, -0.2378, -0.2055, -0.1459, -0.0854, -0.0256, 0.0010, 0.0005, 0.0006, 0.0005, 0.0003, 0.0007, 0.0045, 0.0622, 0.1198, 0.1738, 0.2234, 0.2671, 0.3025, 0.3281, 0.3439, 0.3501, 0.3463, 0.3317, 0.3067, 0.2726, 0.2311, 0.1830, 0.1290, 0.0707, 0.0104, 0.0010, 0.0005, 0.0001, 0.0000, 0.0004, 0.0008, 0.0006, 0.0001, 0.0001, 0.0005, 0.0009, 0.0009, 0.0160, 0.0292, 0.0354, 0.0324, 0.0207, 0.0035, 0.0004, 0.0006, 0.0005, 0.0003, 0.0002, 0.0004, 0.0007, 0.0005, 0.0002, 0.0002, 0.0006, 0.0007, 0.0004, 0.0001, 0.0003, 0.0007, 0.0006, 0.0002, 0.0001, 0.0004, 0.0007, 0.0005, 0.0001, 0.0002, 0.0006, 0.0007, 0.0004, 0.0000, 0.0003, 0.0007, 0.0007, 0.0002, -0.0000, 0.0004, 0.0009, 0.0006};

    /**
     * Currently this test produces a 7 days long measurement with perfect timestamps.
     * The measurement starts with a large positive spike and ends with a large positive spike -
     * the time interval between these two spikes should be roughly 7 days and one second.
     *
     * Note that no emphasis was made to make the timing between spikes exactly 7 days; all
     * segments that are added are rounded in length to nearest packet size.
     */
    @Test
    public void fileStorageTest() {
        init();
        // to see the written file, run from MobECG git repository root directory:
        // `s2debug libs2pcard/src/test/test_measurements/s2PcardStoreOutput.s2`

        String filename = "./src/test/test_measurements/s2PcardStoreOutput.s2";
        assertTrue(prepareOutputFile(filename));
        assertTrue(writeMetadata());
        s2storeStatus.addTextMessage("This is mathematically generated measurement file");
        s2storeStatus.addTextMessage("It contains a spike at beginning and end, and 60Hz artificial ECG in between");
        s2storeStatus.addTextMessage("It also contains 1h holes (missing data) every 12h, starting at hour 5");
        int lengthInDays = 7;
        s2storeStatus.addTextMessage("File length = "+lengthInDays+" days");

        int[] ecg = new int[syntheticECG.length];
        for (int i = 0; i < ecg.length; ++i)
            ecg[i] = (int)((syntheticECG[i] - ltf.getN()) / ltf.getK());

        double endSeconds = 0;
        assertTrue(addSpike());
        for (int i=0; i<2*lengthInDays; ++i) {
            endSeconds += 3600*12;
            // add some samples (total 3600*12*7*2 seconds)
            assertTrue(addSignal(endTimeToLength(endSeconds-3600*7), ecg));
            assertTrue(addHole(endTimeToLength(endSeconds-3600*6)));
            assertTrue(addSignal(endTimeToLength(endSeconds), ecg));
        }
        double ts = data.times[0] * 1e-9;
        assertTrue(addSpike());
        s2storeStatus.addTextMessage("Time between starting and ending spike = "+ts+" seconds");

        System.out.println("Time between starting and ending spike = "+ts+" seconds");

        // close the file
        s2storeStatus.endFile();
    }

    /**
     * Convert the given end time [s] to length [s], from current position in file.
     * This function can be used to calculate segment length (required by functions for adding
     * segments) if one wishes to lengthen the file to a specific length. Returned length is always
     * rounded up.
     *
     * @param endTime the requested total length of file (or end time of measurement)
     * @return the length of segment to be added
     */
    long endTimeToLength(double endTime) {
        return (long)Math.ceil(endTime-data.times[0]*1e-9);
    }

    /**
     * Initialize the member variables.
     */
    public void init() {
        data.sampleCounter = 14;
        data.times = new long[]{0L};
        data.discontinuedSampling=true;
        data.rawSampleCounter = 14;
        data.sampleCounterUnsure = false;
        data.values = null;
        dt = (long)(1e9*14/samplingFrequency);
    }

    /**
     * Add a dummy signal composed of several sine waves
     * @param lengthInSeconds the length of the signal to add
     * @return true if no errors are encountered
     */
    public boolean addDummySignal(long lengthInSeconds) {
        long lengthInPackets = (long)Math.ceil(lengthInSeconds * samplingFrequency / 14);

        double t1 = data.times[0]*1e-9;
        double dt1 = 1/samplingFrequency;
        for (int i=0; i < lengthInPackets; ++i) {
            // set counter
            mbb.setInts(data.rawSampleCounter, 140, 10, 1);
            // set data
            for (int j=0; j<14; ++j) {
                int val = (int)(100 * Math.pow(Math.sin(Math.PI * t1/21)*Math.sin(Math.PI * t1/303), 9) * Math.sin(Math.PI * t1/5))+512;
                mbb.setInts(val, j*10, 10, 1);
                t1 += dt1;
            }

            // write to file
            addPacket(data, rawData);

            // set next counter
            data.rawSampleCounter += 14;
            if (data.rawSampleCounter >= 1024)
                data.rawSampleCounter -= 1024;
            // set next timestamp
            data.times[0] += dt;
        }

        return s2storeStatus.isOk();
    }

    /**
     * Add a dummy signal composed of several sine waves
     * @param lengthInSeconds the length of the signal to add
     * @param signal          the signal to put in (periodically)
     * @return true if no errors are encountered
     */
    boolean addSignal(long lengthInSeconds, int[] signal) {
        long lengthInPackets = (long)Math.ceil(lengthInSeconds * samplingFrequency / 14);

        double t1 = data.times[0]*1e-9;
        double dt1 = 1/samplingFrequency;
        int signalIndex = (int)Math.floor(t1*samplingFrequency) % signal.length;
        for (int i=0; i < lengthInPackets; ++i) {
            // set counter
            mbb.setInts(data.rawSampleCounter, 140, 10, 1);
            // set data
            for (int j=0; j<14; ++j) {
                int val = signal[signalIndex];
                signalIndex = (signalIndex+1) % signal.length;
                mbb.setInts(val, j*10, 10, 1);
                t1 += dt1;
            }

            // write to file
            addPacket(data, rawData);

            // set next counter
            data.rawSampleCounter += 14;
            if (data.rawSampleCounter >= 1024)
                data.rawSampleCounter -= 1024;
            // set next timestamp
            data.times[0] += dt;
        }

        return s2storeStatus.isOk();
    }

    /**
     * Add approx one second of signal that is mostly DC with a spike in the middle
     * @return if no errors are encountered
     */
    public boolean addSpike() {
        long lengthInPackets = (long)Math.ceil(samplingFrequency / 14);

        double t1 = 0;
        double dt1 = 1/samplingFrequency;
        for (int i=0; i < lengthInPackets; ++i) {
            // set counter
            mbb.setInts(data.rawSampleCounter, 140, 10, 1);
            // set data
            for (int j=0; j<14; ++j) {
                int val = (int)(500 * Math.pow(Math.sin(Math.PI * t1)*Math.sin(Math.PI * t1 * 13), 55))+512;
                mbb.setInts(val, j*10, 10, 1);
                t1 += dt1;
            }

            // write to file
            addPacket(data, rawData);

            // set next counter
            data.rawSampleCounter += 14;
            if (data.rawSampleCounter >= 1024)
                data.rawSampleCounter -= 1024;
            // set next timestamp
            data.times[0] += dt;
        }

        return s2storeStatus.isOk();
    }

    /**
     * Add a hole in the data (missing samples)
     * @param lengthInSeconds the length of the signal to add
     * @return true if no errors are encountered
     */
    public boolean addHole(long lengthInSeconds) {
        long lengthInPackets = (long)Math.ceil(lengthInSeconds * samplingFrequency / 14);

        // set next counter
        data.rawSampleCounter += 14*lengthInPackets;
        while (data.rawSampleCounter >= 1024) {
            data.rawSampleCounter -= 1024;
        }
        // set next timestamp
        data.times[0] += dt*lengthInPackets;

        return s2storeStatus.isOk();
    }

    /**
     * Prepare the output file (s2, pcard format)
     * @param name file name, including path
     * @return true if no errors are encountered
     */
    public boolean prepareOutputFile(String name) {
        // create an S2 object (file will also be created, but this should be delegated to after some data is ready to be written - TODO)
        s2file = new S2();
        s2storeStatus = s2file.store(new File(name));

        if (!s2storeStatus.isOk()) {
            System.err.println("Error creating s2 file for storage");
            return false;
        }

        return s2storeStatus.isOk();
    }

    /**
     * Write the metadata to output file.
     * Note that these are mostly hardcoded, but some are available as member variables, that can
     * be set to desired values before calling this function.
     *
     * @return true if no errors are encountered
     */
    public boolean writeMetadata() {
        // File Version
        s2storeStatus.setVersion(S2PcardFile.Version.targetBaseVersion, S2PcardFile.Version.targetExtendedVersion).addMetadata(S2PcardFile.Metadata.Recording_device_software.toString(), "Test script 1.0.0");


        // metadata
        Date today = new Date();

        s2storeStatus
                .addMetadata(S2PcardFile.Metadata.Sensing_gadget_hardware.toString(), "Fake savvy" + "1.0.0")
                .addMetadata(S2PcardFile.Metadata.Mac_address.toString(), "IN:VA:LI:D_:AD")
                .addMetadata(S2PcardFile.Metadata.Recording_date.toString(), new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(today))
                .addMetadata(S2PcardFile.Metadata.Recording_time.toString(), new SimpleDateFormat("HH:mm:ss.SSS", Locale.US).format(today))
                .addMetadata(S2PcardFile.Metadata.Recording_timezone.toString(), new SimpleDateFormat("Z", Locale.US).format(today));

        s2storeStatus.addMetadata(S2PcardFile.Metadata.Selected_adc_channel.toString(), "ECG_OUT");
        s2storeStatus.addMetadata(S2PcardFile.Metadata.Debug_selected_analog_ampl.toString(), "1");
        s2storeStatus.addMetadata(S2PcardFile.Metadata.Debug_selected_ecg_range.toString(), "6");
        s2storeStatus.addMetadata(S2PcardFile.Metadata.Selected_transfer_protocol.toString(), "1");
        s2storeStatus.addMetadata(S2PcardFile.Metadata.Recording_device_model.toString(), "PC");

        // stream definitions
        // define the stream type and use it to generate S2 definitions to ne stored
        S2StreamType s2StreamType = S2StreamType.NormalStream;

        Map<Byte, S2.SensorDefinition> sds = s2StreamType.generateSensorDefinitions(ltf, samplingFrequency);
        for (Map.Entry<Byte, S2.SensorDefinition> sd: sds.entrySet())
            s2storeStatus.addDefinition(sd.getKey(), sd.getValue());
        byte handle = s2StreamType.getStructDefinitionHandle();
        s2storeStatus.addDefinition(handle, s2StreamType.generateStructDefinition());
        timestampDefinitionForDataStream = s2StreamType.generateTimestampDefinition();
        ecgTimestampFormatter = timestampDefinitionForDataStream.newTimestampFormatter();
        s2storeStatus.addDefinition(handle, timestampDefinitionForDataStream);

        return s2storeStatus.isOk();
    }

    /**
     * Add a single packet of streaming data to the output file.
     * @param ecgData an instance of data packet (only .times[0] is required)
     * @param rawData a 19byte buffer already containing raw data samples and sample counter
     *                (normal data stream format)
     */
    public void addPacket(UnpackedEcgData ecgData, byte[] rawData) {
        try {
            // new buffer to store timestamp and samples (rawdata)
            byte buffer[] = new byte[255];
            // copy rawdata to buffer
            for (int i = 0; i < rawData.length; ++i) {
                buffer[i+ecgTimestampFormatter.getTimestampLength()] = rawData[i];
            }
            // format timestamp and copy it to the buffer
            if (!ecgTimestampFormatter.formatForStorage(ecgData.times[0], buffer, writeTimestampBeforeSamples )) {
                // formatForStorage returned true, timestamp line needs to be added manually before the data stream line
                writeTimestampBeforeSamples = false;

                // do not write a timestamp within the data stream packet, add a timestamp (accurate one), then a sensor packet with offset 0
                // note that ecgTimetampFormatter
                // lastReceivedTime = NanoTime.fromLong(ecgData.times[0]);
                s2storeStatus.addTimestamp(new S2.Nanoseconds(ecgTimestampFormatter.getLastTimestamp()-firstReceivedTime));
            }
            s2storeStatus.addSensorPacket((byte) 0, buffer, ecgTimestampFormatter.getTimestampLength() + rawData.length);
        } catch (Exception e) {
            System.err.println("Failed to add a stream packet to s2");
        }
    }
}
