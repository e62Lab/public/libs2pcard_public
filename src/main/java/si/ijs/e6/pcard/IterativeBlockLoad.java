package si.ijs.e6.pcard;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that implements {@link si.ijs.e6.pcard.S2Load.IterativeConvertCallback} with basic
 * functionality - load everything in local arrays or arraylists.
 *
 * This class is used in unit tests and in octave wrapper of the S2Load library.
 */
public class IterativeBlockLoad implements S2Load.IterativeConvertCallback {
    public final long[] countSamples = {0};
    public final float[] samplingFrequency = {0};
    public final long[] timestamps = {0, 0};
    public ArrayList<Float> samples = null;

    /**
     * This function is mainly here for the use in octave; it converts arrayList to plain array.
     * @return samples as primitive float array
     */
    public float[] getSamples() {
        if (samples != null) {
            float[] ret = new float[samples.size()];
            for (int i=0; i < ret.length; i++)
                ret[i] = samples.get(i);
            return ret;
        } else
            return new float[0];
    }

    /**
     * Get relative (measurement start) timestamps [nanoeconds] for the recorded samples
     * @return timetamps (length is equal to getSamples().length)
     */
    public long[] getTimestamps() {
        if (samples != null) {
            int sz = samples.size();
            long[] ret = new long[sz];
            long td = (timestamps[1]-timestamps[0]);
            if (sz > 1) {
                for (int i = 0; i < ret.length; i++)
                    ret[i] = timestamps[0] + (td * i) / (sz - 1);
            } else {
                if (sz == 1)
                    ret[0] = timestamps[0];
            }

            return ret;
        } else
            return new long[0];
    }

    /**
     * Call this function to enable (default is disabled) the storage of all samples.
     * Samples will be stored as one single block in a list {@link #samples}
     */
    public void enableStorageOfSamples() {
        this.samples = new ArrayList<>();
    }

    //region overridden methods
    @Override
    public float selectFrequency(float inputFrequency) {
        samplingFrequency[0] = inputFrequency;
        return inputFrequency;
    }

    @Override
    public void onInitialization(S2Load.FileScanResults scanResult) {
        //System.out.printf("File scan: %ds - %ds\n",
        // (int)(scanResult.startTimeNanos*1e-9), (int)(scanResult.endTimeNanos*1e-9));
        timestamps[0] = scanResult.startTimeNanos;
        timestamps[1] = scanResult.endTimeNanos;
    }

    @Override
    public boolean onIteration(float samples[]) {
        countSamples[0] += samples.length;
        if (this.samples != null)
           this.samples.addAll(asList(samples));
        return S2Load.S2LoadCallback.CONTINUE_READING;
    }

    @Override
    public void complete() {

    }
    //endregion

    /**
     * Helper function used in {@link si.ijs.e6.pcard.S2Load.IterativeConvertCallback#onIteration(float[])} to store samples
     * @param is input array
     * @return List wrapper over the input array
     */
    private List<Float> asList(final float[] is) {
        return new AbstractList<Float>() {
            public Float get(int i) {
                return is[i];
            }

            public int size() {
                return is.length;
            }
        };
    }
}
